var Joi = require('joi');
var Boom = require('boom');
var db = require('../models')
var User = db['admin_user'];

exports.register = function (server, options, next) {

  server.route({
    method: 'GET',
    path: '/user',
    config: {
      description: 'Busca de Locais',
      validate: {
        query: {
          filtro: Joi.string().default("{}").description('Filtros para a Consulta'),
        }
      }
    },
    handler: function (request, reply) {
      const parametros = JSON.parse(request.query.filtro)
      User.findAll({
        where: parametros
      }).then((user) => {
        reply(user);
      });
    }
  });

  server.route({
    method: 'POST',
    path: '/user',
    config: {
      description: 'Busca de Locais',
      validate: {
        payload: {
          username: Joi.string().description('Filtros para a Consulta'),
          user_password: Joi.string().description('Filtros para a Consulta'),
          full_name: Joi.string().description('Filtros para a Consulta'),
        }
      }
    },
    handler: function (request, reply) {
      return db.sequelize.transaction({
        isolationLevel: db.Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED
      }, function(t) {
        return User.create(request.payload, { transaction: t }).then(function(user) {
          return user.get({
            plain: true
          })
        })
      })
      .then(function(user) {
        reply(user)
      })
      .catch(function(err) {
        const error = Boom.badRequest(err);
        reply(error);
      });
    }
  });

  return next();

}

exports.register.attributes = {
    name: 'adminUser',
    version: require('../package.json').version
};