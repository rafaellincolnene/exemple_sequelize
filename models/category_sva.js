/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('category_sva', {
    id_category: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'category',
        key: 'id_category'
      }
    },
    id_sva: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'sva',
        key: 'id_sva'
      }
    }
  }, {
    tableName: 'category_sva'
  });
};
