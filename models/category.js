/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('category', {
    id_category: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    category_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    show_home: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    show_menu: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    order_home: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    order_category: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    tableName: 'category'
  });
};
