/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('banner', {
    id_banner: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    url_image_sm: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url_image_md: {
      type: DataTypes.STRING,
      allowNull: false
    },
    url_image_lg: {
      type: DataTypes.STRING,
      allowNull: false
    },
    start_date: {
      type: DataTypes.TIME,
      allowNull: true
    },
    end_date: {
      type: DataTypes.TIME,
      allowNull: true
    }
  }, {
    tableName: 'banner'
  });
};
