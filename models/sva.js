/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sva', {
    id_sva: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    product_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    page_url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: false
    },
    code_product_weekly: {
      type: DataTypes.STRING,
      allowNull: true
    },
    code_product_monthly: {
      type: DataTypes.STRING,
      allowNull: true
    },
    free: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    cell_phone: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    phone: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    internet: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    television: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'sva'
  });
};
